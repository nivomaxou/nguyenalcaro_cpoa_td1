package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Connections.*;
import Metier.*;
import ListeMemoire.*;

public class ClientDAO extends Connexion implements DAO<Client> {
	
	@Override
	public Client getById(int id) {
		// TODO Auto-generated method stub
		
		Client client = null;
		try {
			Connection laConnexion =  creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("select * from Client where id_client=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();
			while(res.next()){
				client.setid_client(res.getInt("id_client")); 
				client.setNom(res.getString("nom"));
				client.setPrenom(res.getString("prenom"));
				client.setNo_rue(res.getString("no_rue"));
				client.setVoie(res.getString("voie"));
				client.setcode_postal(res.getString("code_postal"));
				client.setVille(res.getString("ville"));
				client.setPays(res.getString("pays"));
				res.close();
				}
		} catch (SQLException sqle) {
			System.out.println("Erreur connexion" + sqle.getMessage());}
		return client;
	}

	
	@Override
	public void create(Client objet) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("insert into Client (id_client, nom, prenom, no_rue, voie, code_postal, ville, pays) VALUES ( ?,?,?,?,?,?,?,? )");			
			
			requete.setString(1, objet.getNom());
			requete.setString(2, objet.getPrenom());
			requete.setString(3, objet.getNo_rue());
			requete.setString(4, objet.getVoie());
			requete.setString(5, objet.getcode_postal());
			requete.setString(6, objet.getVille());
			requete.setString(7, objet.getPays());  
			requete.executeUpdate();
			laConnexion.close();
			} catch (SQLException sqle) {System.out.println("Pb dans select " + sqle.getMessage());}
		
		
	}

	@Override
	public void update(Client objet) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("update Client set nom = ?, set prenom = ?, set no_rue = ?, voie = ?, code_postal = ?, ville = ?, pays = ? where id_client = ?");
			
			
			requete.setString(1, objet.getNom());
			requete.setString(2, objet.getPrenom());
			requete.setString(3, objet.getNo_rue());
			requete.setString(4, objet.getVoie());
			requete.setString(5, objet.getcode_postal());
			requete.setString(6, objet.getVille());
			requete.setString(7, objet.getPays()); 
			requete.executeUpdate();
			laConnexion.close();
			} catch (SQLException sqle) {System.out.println("Pb dans select " + sqle.getMessage());}
	
		
	}

	@Override
	public void delete(Client objet) {
		// TODO Auto-generated method stub
		
		try {
			Connection laConnexion = creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("delete from Client where id_client = ?");
			requete.setInt(1, objet.getid_client());
			requete.executeUpdate();
			laConnexion.close();} 
		catch (SQLException sqle) {System.out.println("Pb dans select " + sqle.getMessage());}

		
		
	}


	@Override
	public Periodicite getByIdPeriodicite() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public ArrayList<Revue> findAll() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Revue getByIdRevue() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<Integer> findRevue() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Client getByIdClient() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<Client> findClient() {
		// TODO Auto-generated method stub
		return null;
	}


}