package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Connections.*;
import Metier.*;
import ListeMemoire.*;

public class RevueDAO extends Connexion implements DAO<Revue> {
	
	@Override
	public Revue getById(int id) {
		// TODO Auto-generated method stub
		
		Revue revue = null;
		try {
			Connection laConnexion =  creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("select * from Client where id_revue=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();
			while(res.next()){
			revue.setid_revue(res.getInt("id_revue"));
			revue.setTitre(res.getString("titre")); 
			revue.setDescription(res.getString("description")); 
			revue.settarif_numero(res.getInt("tarif_numero")); 
			revue.setVisuel(res.getString("visuel")); 
			revue.setId_periodicite(res.getInt("id_periodicite")); 
			}
		} catch (SQLException sqle) {
			System.out.println("Erreur connexion" + sqle.getMessage());} 
		return revue;	}

	

	@Override
	public void create(Revue objet) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("insert into Revue (id_revue,titre,description,tarif_numero,visuel, id_periodicite) VALUES ( ?,?,?,?,?,? )");
			requete.setString(1, objet.getTitre());
			requete.setString(2, objet.getDescription());
			requete.setFloat(3, objet.gettarif_numero());
			requete.setString(4, objet.getVisuel());
			requete.setInt(5, objet.getId_periodicite());
			requete.executeUpdate();
			laConnexion.close();
			} catch (SQLException sqle) {System.out.println("Pb dans select " + sqle.getMessage());}

		
	}

















	@Override
	public void update(Revue objet) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("update Revue set titre = ?, description = ?, tarif_numero = ?, visuel = ?, id_periodicite = ? where id_revue = ?");
			requete.setString(1, objet.getTitre());
			requete.setString(2, objet.getDescription());
			requete.setFloat(3, objet.gettarif_numero());
			requete.setString(4, objet.getVisuel());
			requete.setInt(5, objet.getId_periodicite());
			requete.executeUpdate();
			laConnexion.close();
			} catch (SQLException sqle) {System.out.println("Pb dans select " + sqle.getMessage());}
		
	}

















	@Override
	public void delete(Revue objet) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("delete from Revue where id_revue = ?");
			requete.setInt(1, objet.getid_revue());
			requete.executeUpdate();
			laConnexion.close();} catch (SQLException sqle) {System.out.println("Pb dans select " + sqle.getMessage());}
		
	}



	@Override
	public Periodicite getByIdPeriodicite() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public ArrayList<Revue> findAll() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public Revue getByIdRevue() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<Integer> findRevue() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public Client getByIdClient() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<Client> findClient() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	

}