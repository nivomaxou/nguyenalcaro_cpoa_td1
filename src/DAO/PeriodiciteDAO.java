package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Connections.*;
import Metier.*;
import ListeMemoire.*;

public class PeriodiciteDAO extends Connexion implements DAO<Periodicite> {
	
	@Override
	public Periodicite getById(int id) {
		// TODO Auto-generated method stub
		
		Periodicite periodicite = null;
		try {
			Connection laConnexion =  creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("select * from Client where id_periodicite=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();
			while(res.next()){
				periodicite.setid_periodicite(res.getInt("id_periodicite")); 
				periodicite.setlibelle(res.getString("libelle")); 
			}
		} catch (SQLException sqle) {
			System.out.println("Erreur connexion" + sqle.getMessage());} 
		return periodicite;	}
	

	
	@Override
	public void create(Periodicite objet) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("insert into Periodicite (id_periodicite, libelle) VALUES ( ?,? )");

			requete.setString(1, objet.getlibelle());
			requete.executeUpdate();
			laConnexion.close();
			} catch (SQLException sqle) {System.out.println("Pb dans select " + sqle.getMessage());}

		
	}













	@Override
	public void update(Periodicite objet) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("update Periodicite set libelle = ? where id_periodicite = ?");
			requete.setString(1, objet.getlibelle());
			requete.executeUpdate();
			laConnexion.close();} catch (SQLException sqle) {System.out.println("Pb dans select " + sqle.getMessage());}
		
	}













	@Override
	public void delete(Periodicite objet) {
		// TODO Auto-generated method stub
		
		try {
			Connection laConnexion = creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("delete from Periodicite where id_periodicite = ?");
			requete.setInt(1, objet.getid_periodicite());
			requete.executeUpdate();
			laConnexion.close();} catch (SQLException sqle) {System.out.println("Pb dans select " + sqle.getMessage());}
	}



	@Override
	public Periodicite getByIdPeriodicite() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public ArrayList<Revue> findAll() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public Revue getByIdRevue() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<Integer> findRevue() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public Client getByIdClient() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<Client> findClient() {
		// TODO Auto-generated method stub
		return null;
	}


		
	
}