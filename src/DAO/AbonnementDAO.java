package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Connections.*;
import Metier.*;
import ListeMemoire.*;

public class AbonnementDAO extends Connexion implements DAO<Abonnement> {
	
	@Override
	public Abonnement getById(int id_client) {
		// TODO Auto-generated method stub
		
		Abonnement abonnement = null;
		try {
			Connection laConnexion =  creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("select * from Client where id_client=?");
			requete.setInt(1, id_client);
			ResultSet res = requete.executeQuery();
			while(res.next()){
				abonnement.setId_client(res.getInt("id_client")); 
				abonnement.setId_revue(res.getInt("id_revue")); 
				abonnement.setDate_debut(res.getDate("date_debut")); 
				abonnement.setDate_debut(res.getDate("date_fin")); 

			}
		} catch (SQLException sqle) {
			System.out.println("Erreur connexion" + sqle.getMessage());} 
		return abonnement;	}

	
	
	
	
	
	@Override
	public void create(Abonnement objet) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("insert into Abonnement (id_client, id_revue, date_debut,date_fin) VALUES ( ?,?,?,? )");
			requete.setInt(1, objet.getId_revue());
			requete.setDate(2, objet.getDate_debut());
			requete.setDate(3, objet.getDate_debut());
			requete.executeUpdate();
			laConnexion.close();
			} catch (SQLException sqle) {
				System.out.println("Pb dans select " + sqle.getMessage());}
		
	}

	@Override
	public void update(Abonnement objet) {
		// TODO Auto-generated method stub
		try {
		Connection laConnexion = creeConnexion(); 
		PreparedStatement requete = laConnexion.prepareStatement("update Abonnement set date_debut = ?, set date_fin = ? where id_client = ? AND id_revue = ?");
		requete.setInt(1, objet.getId_revue());
		requete.setDate(2, objet.getDate_debut());
		requete.setDate(3, objet.getDate_debut());
		requete.executeUpdate();
		
		laConnexion.close();
		} catch (SQLException sqle) {
			System.out.println("Pb dans select " + sqle.getMessage());}
	}

	@Override
	public void delete(Abonnement objet) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("delete from Abonnement where id_client = ? AND id_revue = ?");
			requete.setInt(1, objet.getId_client());
			requete.setInt(1, objet.getId_revue());
			requete.executeUpdate();
			laConnexion.close();
			} catch (SQLException sqle) {
				System.out.println("Pb dans select " + sqle.getMessage());}
		
	}






	@Override
	public Periodicite getByIdPeriodicite() {
		// TODO Auto-generated method stub
		return null;
	}






	@Override
	public ArrayList<Revue> findAll() {
		// TODO Auto-generated method stub
		return null;
	}






	@Override
	public Revue getByIdRevue() {
		// TODO Auto-generated method stub
		return null;
	}






	@Override
	public List<Integer> findRevue() {
		// TODO Auto-generated method stub
		return null;
	}






	@Override
	public Client getByIdClient() {
		// TODO Auto-generated method stub
		return null;
	}






	@Override
	public List<Client> findClient() {
		// TODO Auto-generated method stub
		return null;
	}

	

}