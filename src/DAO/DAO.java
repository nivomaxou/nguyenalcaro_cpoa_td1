package DAO;

import java.util.ArrayList;
import java.util.List;

import Metier.Client;
import Metier.Periodicite;
import Metier.Revue;

public interface DAO<T> {
	
		public abstract T getById(int id);
		
		public abstract void create(T objet);
		public abstract void update(T objet);
		public abstract void delete(T objet);

		Periodicite getByIdPeriodicite();

		ArrayList<Revue> findAll();

		Revue getByIdRevue();

		List<Integer> findRevue();

		Client getByIdClient();

		List<Client> findClient();

}
