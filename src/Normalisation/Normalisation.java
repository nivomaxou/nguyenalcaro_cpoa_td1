package Normalisation;

public class Normalisation {
	
	public static String NormalisationPays(String Pays){
		String Pays2 = Pays.trim();
		Pays2 = Pays.toLowerCase();
		
		switch(Pays2){
		case "letzebuerg":
		case "luxembourg":
			Pays2 = "Luxembourg";
			break;
			
			
		case "belgium":
		case "belgique":
			Pays2 = "Belgique";
			break;
			
		case "switzerland":
		case "schweiz":
		case "suisse":
			Pays2 = "Suisse";
			break;
			
		}
		return Pays2;
	}
	
	
	
	
	public  static String NormalisationVille(String Ville){
		String Ville2 = " ";
		
		
		if ((Ville.contains(" le ") || Ville.contains(" l�s ") || Ville.contains(" sous ") || Ville.contains(" sur ") || Ville.contains(" � ") || Ville.contains(" aux ") || Ville.contains(" les ")))
		{
			Ville2=Ville.replace(' ','-');
		}
		else
		{
			Ville2=Ville;
		}
		
		if (Ville2.contains("St ") || Ville2.contains("st ") || Ville2.contains("St-") || Ville2.contains("st-") || Ville2.contains("Ste ") || Ville2.contains("ste ") || Ville2.contains("Ste-") || Ville2.contains("ste-"))
		{		
			Ville2 = Ville2.replace("Ste-","Sainte-");
			Ville2 = Ville2.replace("St-","Saint-");
			Ville2 = Ville2.replace("ste-","Sainte-");
			Ville2 = Ville2.replace("st-","Saint-");

		}
		String Ville3 = Ville2.trim();
		return Ville3;

	}
	
	public  static String NormalisationVoie(String Voie){
		
		String Voie2 = Voie.toLowerCase();
		String Voie3 = " ";
		
		if (Voie2.contains ("boul "))
		{
			
			Voie3 = Voie2.replace("boul ","Boulevard ");
		}
		else if (Voie2.contains("boul. ")){ 
			Voie3 = Voie2.replace("boul. ","Boulevard ");
		}
		
		else if (Voie2.contains("bd.")){
			Voie3 = Voie2.replace("bd. ","Boulevard ");
		}
		
		else if (Voie2.contains ("av "))
		{
			
			Voie3 = Voie2.replace("av ","Avenue ");
		}
		
		else if (Voie2.contains ("fg "))
		{
			
			Voie3 = Voie2.replace("fg ","Faubourg ");
		}
		
		else if (Voie2.contains ("faub "))
		{
			Voie3 = Voie2.replace("faub ","Faubourg ");
		}
		else
		{ 
			
			Voie3=Voie2;
		}

		String Voie4 = Voie3.trim();
		return Voie4;

	}
	
	
	public  static String NormalisationCodePostal(String codepostal){
		String codepostal2 = " ";
		String codepostal3 = " ";
		codepostal2 = codepostal.trim();
		int longueurcodepostal = codepostal2.length();
		
		if ((codepostal2.contains("L-"))){
			codepostal3 = codepostal2.replace("L-","");
		}
		else 
		if ((longueurcodepostal<5)){
			codepostal3 = "0"+codepostal2; 
		}
		
		return codepostal3;
	}
	
	
	public  static String NormalisationRue(String no_rue, String adresse){
	String adresse2;
	adresse2 = no_rue +", "+ adresse;
	
	return adresse2;
}

}

