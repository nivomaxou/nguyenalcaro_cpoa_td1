package ListeMemoire;

import java.util.ArrayList;
import java.util.List;

import DAO.RevueDAO;
import Metier.Revue;

public class ListeMemoireRevue extends RevueDAO {

	private static ListeMemoireRevue instance;

	private List<Revue> donnees;


	public static ListeMemoireRevue getInstance() {

		if (instance == null) {
			instance = new ListeMemoireRevue();
		}

		return instance;
	}
	
	private ListeMemoireRevue() {

		this.donnees = new ArrayList<Revue>();

		this.donnees.add(new Revue(1, "Titre", "Description", 2, "Visuel", 3));

		
	}


		
		


	@Override
	public void create(Revue objet) {

		objet.setid_revue(3);

		// Ne fonctionne que si l'objet métier est bien fait...
		while (this.donnees.contains(objet)) {

			objet.setid_revue(objet.getid_revue() + 1);
		}
	}

	@Override
	public void update(Revue objet) {

		// Ne fonctionne que si l'objet métier est bien fait...
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			this.donnees.set(idx, objet);
		}
	}

	@Override
	public void delete(Revue objet) {

		// Ne fonctionne que si l'objet métier est bien fait...
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			this.donnees.remove(idx);
		}
	}

	@Override
	public Revue getById(int id) {

		// Ne fonctionne que si l'objet métier est bien fait...
		int idx = this.donnees.indexOf(new Revue(id, "Titre", "Description", id, "Visuel", id));
		if (idx == -1) {
			throw new IllegalArgumentException("Aucun objet ne possède cet identifiant");
		} else {
			return this.donnees.get(idx);
		}
	}

	public List<Revue> findAll() {

		return this.donnees;
	}

}

