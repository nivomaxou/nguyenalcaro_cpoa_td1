package ListeMemoire;

import java.util.ArrayList;
import java.util.List;

import DAO.ClientDAO;
import Metier.Client;

public class ListeMemoireClient extends ClientDAO {

	private static ListeMemoireClient instance;

	private List<Client> donnees;


	public static ListeMemoireClient getInstance() {

		if (instance == null) {
			instance = new ListeMemoireClient();
		}

		return instance;
	}

	private ListeMemoireClient() {


		this.donnees.add(new Client(1, "Nom", " Prenom", "No Rue", "Voie", "Code Postal", "Ville", "Pays"));
	}


	@Override
	public void create(Client objet) {

		objet.setid_client(3);

		// Ne fonctionne que si l'objet métier est bien fait...
		while (this.donnees.contains(objet)) {

			objet.setid_client(objet.getid_client() + 1);
		}
	}

	@Override
	public void update(Client objet) {

		// Ne fonctionne que si l'objet métier est bien fait...
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			this.donnees.set(idx, objet);
		}
	}

	@Override
	public void delete(Client objet) {

		// Ne fonctionne que si l'objet métier est bien fait...
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			this.donnees.remove(idx);
		}
	}

	@Override
	public Client getById(int id) {

		// Ne fonctionne que si l'objet métier est bien fait...
		int idx = this.donnees.indexOf(new Client(id, "test", " test", "test", "test", "test", "test", "test"));
		if (idx == -1) {
			throw new IllegalArgumentException("Aucun objet ne possède cet identifiant");
		} else {
			return this.donnees.get(idx);
		}
	}

	public List<Client> findAll() {

		return this.donnees;
	}

}

