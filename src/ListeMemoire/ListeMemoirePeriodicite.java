package ListeMemoire;

import java.util.ArrayList;
import java.util.List;

import DAO.PeriodiciteDAO;
import Metier.Periodicite;

public class ListeMemoirePeriodicite extends PeriodiciteDAO {

	private static ListeMemoirePeriodicite instance;

	private List<Periodicite> donnees;


	public static ListeMemoirePeriodicite getInstance() {

		if (instance == null) {
			instance = new ListeMemoirePeriodicite();
		}

		return instance;
	}

	private ListeMemoirePeriodicite() {

		this.donnees = new ArrayList<Periodicite>();

		this.donnees.add(new Periodicite(1, "Mensuel"));
		this.donnees.add(new Periodicite(2, "Quotidien"));
	}


	@Override
	public void create(Periodicite objet) {

		objet.setid_periodicite(3);

		// Ne fonctionne que si l'objet métier est bien fait...
		while (this.donnees.contains(objet)) {

			objet.setid_periodicite(objet.getid_periodicite() + 1);
		}
	}

	@Override
	public void update(Periodicite objet) {

		// Ne fonctionne que si l'objet métier est bien fait...
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			this.donnees.set(idx, objet);
		}
	}

	@Override
	public void delete(Periodicite objet) {

		// Ne fonctionne que si l'objet métier est bien fait...
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			this.donnees.remove(idx);
		}
	}

	@Override
	public Periodicite getById(int id) {

		// Ne fonctionne que si l'objet métier est bien fait...
		int idx = this.donnees.indexOf(new Periodicite(id, "test"));
		if (idx == -1) {
			throw new IllegalArgumentException("Aucun objet ne possède cet identifiant");
		} else {
			return this.donnees.get(idx);
		}
	}

	public List<Periodicite> findAll() {

		return this.donnees;
	}

}

