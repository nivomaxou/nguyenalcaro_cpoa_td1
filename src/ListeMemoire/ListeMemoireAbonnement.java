package ListeMemoire;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import DAO.AbonnementDAO;
import Metier.Abonnement;

public class ListeMemoireAbonnement extends AbonnementDAO {

	private static ListeMemoireAbonnement instance;

	private List<Abonnement> donnees;


	public static ListeMemoireAbonnement getInstance() {

		if (instance == null) {
			instance = new ListeMemoireAbonnement();
		}

		return instance;
	}

	private ListeMemoireAbonnement() {

		this.donnees = new ArrayList<Abonnement>();

		this.donnees.add(new Abonnement(1, 2, new Date(2017,11,10), new Date(2017,12,10)));
	}


	@Override
	public void create(Abonnement objet) {

		objet.setId_client(3);

		// Ne fonctionne que si l'objet métier est bien fait...
		while (this.donnees.contains(objet)) {

			objet.setId_client(objet.getId_client() + 1);
		}
	}

	@Override
	public void update(Abonnement objet) {

		// Ne fonctionne que si l'objet métier est bien fait...
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			this.donnees.set(idx, objet);
		}
	}

	@Override
	public void delete(Abonnement objet) {

		// Ne fonctionne que si l'objet métier est bien fait...
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			this.donnees.remove(idx);
		}
	}

	@Override
	public Abonnement getById(int id) {

		// Ne fonctionne que si l'objet métier est bien fait...
		int idx = this.donnees.indexOf(new Abonnement(id, id, date, date));
		if (idx == -1) {
			throw new IllegalArgumentException("Aucun objet ne possède cet identifiant");
		} else {
			return this.donnees.get(idx);
		}
	}

	public List<Abonnement> findAll() {

		return this.donnees;
	}

}

