package Vue;

import java.net.URL;

import Controleur.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;


public class VueClient extends Stage{
 
	
	public VueClient() {
		try {
			 final URL fxmlURL=getClass().getResource("/Vue/clients.fxml");
			 final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
			 final VBox node = (VBox)fxmlLoader.load();
			 Scene scene = new Scene(node);
		
			this.setScene(scene);
			this.setTitle("Clients");
		
			
			ControleClient controleur = fxmlLoader.getController();
			controleur.setVue(this);
			
			
			this.initModality(Modality.APPLICATION_MODAL);
			this.setResizable(false);
			this.show();
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}