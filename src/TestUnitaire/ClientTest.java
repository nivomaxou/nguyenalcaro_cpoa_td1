package TestUnitaire;
import Normalisation.*;

import static org.junit.Assert.*;

import org.junit.Test;


	/* TESTE PAYS */


public class ClientTest {

	private Normalisation norme;
	
	@Test
	public void testPaysletzebuerg() {
		assertEquals("Luxembourg", norme.NormalisationPays("letzebuerg"));
	}
	
	@Test
	public void testPaysBelgium() {
		assertEquals("Belgique", norme.NormalisationPays("Belgium"));
	}
	
	@Test
	public void testPaysswitzerland() {
		assertEquals("Suisse", norme.NormalisationPays("switzerland"));
	}
	
	@Test
	public void testPaysschweiz() {
		assertEquals("Suisse", norme.NormalisationPays("schweiz"));
	}
	
	@Test
	public void testPaysMaj() {
		assertEquals("Suisse", norme.NormalisationPays("SUISSE"));
	}
	
	
	
	/* TEST VILLE */
	
	
	@Test
	public void testVille() {
		assertEquals("Thionville", norme.NormalisationVille("   Thionville"));
	}
	
	@Test
	public void testVillest() {
		assertEquals("Saint-Julien-les-Metz", norme.NormalisationVille("st Julien les Metz"));
	}
	
	@Test
	public void testVilleste() {
		assertEquals("Sainte-Marie-aux-Ch�nes", norme.NormalisationVille("ste Marie aux Ch�nes"));
	}
	
	@Test
	public void testVilleSt() {
		assertEquals("Saint-Julien-les-Metz", norme.NormalisationVille("St Julien les Metz"));
	}
	
	@Test
	public void testVilleSte() {
		assertEquals("Sainte-Marie-aux-Ch�nes", norme.NormalisationVille("Ste Marie aux Ch�nes"));
	}
	
	
	
	
	

	/* TESTE VOIE */
	
	@Test
	public void testVoieBoul() {
		assertEquals("Boulevard saint honor�", norme.NormalisationVoie("boul saint honor�"));
	}
	
	@Test
	public void testVoieBd() {
		assertEquals("Boulevard saint honor�", norme.NormalisationVoie("bd. saint honor�"));
	}
	
	
	@Test
	public void testVoieAv() {
		assertEquals("Avenue saint honor�", norme.NormalisationVoie("av saint honor�"));
	}
	
	@Test
	public void testVoieFg() {
		assertEquals("Faubourg saint honor�", norme.NormalisationVoie("fg saint honor�"));
	}
	
	
	@Test
	public void testVoieFaub() {
		assertEquals("Faubourg saint honor�", norme.NormalisationVoie("faub saint honor�"));
	}
	
	
	
	
	
	
	/* TESTE CODE POSTAL */
	
	public void testCodePostal() {
		assertEquals("2270", norme.NormalisationCodePostal("L-2270"));
	}	
	
	@Test
	public void testCodePostal2() {
		assertEquals("08200", norme.NormalisationCodePostal("8200"));
	}
	
	
	
	
	
	
	
	/* TESTE RUE */
	
	@Test
	public void testRue() {
		assertEquals("3, rue des alouettes", norme.NormalisationRue("3","rue des alouettes"));
	}
	
	
}
