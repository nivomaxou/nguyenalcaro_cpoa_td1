package Metier;

public class Client {
	private static String id_client; 
	private String nom;
	private String prenom;
	private String no_rue;
	private String voie;
	private String code_postal;
	private String ville;
	private String pays;
	
	public boolean equals(Object objet) {
		if (this == objet)
			return true;
		if (objet == null)
			return false;
		if (getClass() != objet.getClass())
			return false;
		Client other = (Client) objet;
		if (code_postal == null) {
			if (other.code_postal != null)
				return false;
		} else if (!code_postal.equals(other.code_postal))
			return false;
		if (getid_client() != other.getid_client())
			return false;
		if (no_rue == null) {
			if (other.no_rue != null)
				return false;
		} else if (!no_rue.equals(other.no_rue))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (pays == null) {
			if (other.pays != null)
				return false;
		} else if (!pays.equals(other.pays))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (ville == null) {
			if (other.ville != null)
				return false;
		} else if (!ville.equals(other.ville))
			return false;
		if (voie == null) {
			if (other.voie != null)
				return false;
		} else if (!voie.equals(other.voie))
			return false;
		return true;
	}

	public Client(String id_client, String nom, String prenom, String no_rue, String voie, String code_postal, String ville,
			String pays) {
		super();
		this.id_client = id_client;
		this.nom = nom;
		this.prenom = prenom;
		this.no_rue = no_rue;
		this.voie = voie;
		this.code_postal = code_postal;
		this.ville = ville;
		this.pays = pays;
	}
	
	
	public Client(String id_client2) {
		// TODO Auto-generated constructor stub
		Client.setid_client(id_client2);
	}
	public String getid_client() {
		return getid_client();
	}
	public static void setid_client(String id_client2) {
		Client.setid_client(id_client2);
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNo_rue() {
		return no_rue;
	}
	public void setNo_rue(String no_rue) {
		this.no_rue = no_rue;
	}
	public String getVoie() {
		return voie;
	}
	public void setVoie(String voie) {
		this.voie = voie;
	}
	public String getcode_postal() {
		return code_postal;
	}
	public void setcode_postal(String code_postal) {
		this.code_postal = code_postal;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}


}