package Controleur;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import DAO.*;
import Metier.*;
import Normalisation.*;
import sql.*;
import Vue.*;
import Controleur.*;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class ControleClient implements Initializable{
	
	boolean erreur; 
	private Stage vue; 
	private ControleAccueil verifPer; 

	
	/*public Persistance persi;*/
	
	
	@FXML
	private TextField textfield_numero;
	
	@FXML
	private TextField textfield_nom;

	@FXML
	private TextField textfield_prenom;
	
	@FXML
	private TextField textfield_adresse;
	
	
	@FXML 
	private TableView<Client> tview_clients;
	
	@FXML 
	private TableColumn<Client, String> tc_numero;
	
	@FXML 
	private TableColumn<Client, String> tc_nom;
	
	@FXML 
	private TableColumn<Client, String> tc_prenom;
	
	@FXML 
	private TableColumn<Client, String> tc_adresse;

	@FXML
	private Button btn_client_retour;
	
	@FXML 
	private Button btn_deleteLine; 
	
	@FXML 
	private Button btn_ajouter;
	
	//private Revue rev; 
	
	
	public ControleClient() { 
	}
	
	public Stage getVue() {
		return vue;
	}

	public void setVue(Stage vue) {
		this.vue = vue;
	}


	public void initialize(URL location, ResourceBundle resources) {
		
		//verifPer = new ControleAccueil(); 
		//persi = verifPer.getPers(); 
		
		
		// this.tview_client.getItems().addAll(DAOFactory.getDAOFactory(persi).getRevueDAO().findAll()); 
		
		//tc_numero = new TableColumn<Client, String>("Numero");
		//tc_numero.setCellValueFactory(new PropertyValueFactory<Client, String>("Numero"));
		
		//tc_nom = new TableColumn<Client, String>("Nom");
		//tc_nom.setCellValueFactory(new PropertyValueFactory<Client, String>("Nom"));
		
		//tc_prenom = new TableColumn<Client, String>("Prenom");
		//tc_prenom.setCellValueFactory(new PropertyValueFactory<Client, String>("Prenom"));
		
		//tc_adresse = new TableColumn<Client, String>("Adresse");
		//tc_adresse.setCellValueFactory(new PropertyValueFactory<Client, String>("Adresse"));
		
	
		//this.btn_deleteLine.setDisable(true);
		
		//this.tview_clients.getSelectionModel().selectedItemProperty().addListener(
		//		 (observable, oldValue, newValue) -> {
		//		 this.btn_deleteLine.setDisable(newValue == null);
		//		 });
		
		//tview_clients.getColumns().setAll(tc_numero, tc_nom, tc_prenom, tc_adresse); 
	 
	}
				
		
		/*//methode qui permet de ne permettre la saisie que de nombre et d'un point REF : http://tech.chitgoks.com/2014/10/08/how-to-create-a-numbers-only-textfield-in-java-fx-2/
		textfield_tarif.lengthProperty().addListener(new ChangeListener<Number>(){
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				  if (newValue.intValue() > oldValue.intValue()) {
					  char ch = textfield_tarif.getText().charAt(oldValue.intValue());
					  // Check if the new character is the number or other's
					  if (!(ch >= '0' && ch <= '9' || ch == '.')) {
						   // if it's not number then just setText to previous one
						  textfield_tarif.setText(textfield_tarif.getText().substring(0,textfield_tarif.getText().length()-1)); 
					  }
				 }
			}

		});
		
		DAOFactory dao = DAOFactory.getDAOFactory(persi);
		cb_periodicite.setItems(FXCollections.observableArrayList(dao.getPeriodiciteDAO().findAll()));
		
		cb_periodicite.getSelectionModel().selectFirst();
		
	}
	*/
		
	public void ajouterClient(){
		Client cli = new Client(null); 
		
		Client.setid_client(textfield_numero.getText().trim());
		cli.setNom(textfield_nom.getText().trim());
		cli.setPrenom(textfield_prenom.getText().trim());
		cli.setVoie(textfield_adresse.getText().trim());
		
	

		/*DAOFactory.getDAOFactory(persi).getRevueDAO().create(rev);*/
		
		this.tview_clients.getItems().clear();
		/*this.tview_revues.getItems().addAll(DAOFactory.getDAOFactory(persi).getRevueDAO().findAll()); */

}
	
	public void closeVue() { 
		this.vue.close();
	}
	
	public void deleteLine() {
		Client cli2 = new Client(null);
		
		int selectedItem = tview_clients.getSelectionModel().getSelectedIndex(); 
		//cli2.setid_client(selectedItem +1); 
		if (selectedItem >= 0) {
			tview_clients.getItems().remove(selectedItem);
			/*DAOFactory.getDAOFactory(persi).getRevueDAO().delete(rev2);*/
		}
		else {
			Alert alerte = new Alert(AlertType.WARNING);
			alerte.initOwner(this.vue); //type window
	        alerte.setTitle("Aucune selection.");
	        alerte.setHeaderText("Aucune revue sélectionnée.");
	        alerte.setContentText("Selectionnez une revue.");
	        alerte.showAndWait();
		}
		 
		
	}
	
}
