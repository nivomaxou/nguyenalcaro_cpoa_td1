package Controleur;

import java.net.URL;
import java.util.ResourceBundle;

import Vue.VueAbonnement;
import Vue.VueClient;
import Vue.VuePeriodicite;
import Vue.VueRevue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ControleAccueil implements Initializable  {
	
	private Stage vue; 
	@FXML
	private ComboBox<String> choix_accueil;
	
	@FXML
	private Button btn_abonnements;

	@FXML
	private Button btn_clients;
	
	@FXML
	private Button btn_periodicites;
	
	@FXML
	private Button btn_revues;
	
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		choix_accueil.setItems(FXCollections.observableArrayList("MySQL","Bdd"));
	}
	
	public void affiche_abonnement(){
		VueAbonnement vueabo = new VueAbonnement(); 
	}
	
	public void affiche_clients(){
			VueClient vc = new VueClient();
	}
	
	public void affiche_periodicites(){
		VuePeriodicite vp = new VuePeriodicite(); 
		
	}
	
	public void affiche_revues(){
		
		VueRevue vr = new VueRevue(); 
	}

	public Stage getVue() {
		return vue;
	}

	public void setVue(Stage vue) {
		this.vue = vue;
	}
}
