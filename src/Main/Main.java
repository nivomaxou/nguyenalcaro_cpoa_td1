package Main;

import java.sql.Connection;
import java.sql.Date;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import DAO.*;

import Metier.*;


public class Main{


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		boolean recommencer = false;
		int action = 0;
		int id_periodicite;
		int id_revue;
		int id_client;
		
		float tarifnum_revue;
		
		
		String lib_period;
		String nom_client;
		String prenom_client;
		String norue_client;
		String voie_client;
		String cp_client;
		String ville_client;
		String pays_client;
		String titre_revue;
		String desc_revue;
		String visuel_revue;
		String date_debut;
		String date_fin;
		
		
		while (!recommencer){
		System.out.println(""
				+ "\n"
				+ "\n"
				+ "----------------------------Que voulez vous faire ?----------------------------\n\n");
		System.out.println("----------------------------Abonnement-----------------------------------------");
		System.out.println("1) Ajouter un Abonnement");
		System.out.println("2) Modifier un Abonnement");
		System.out.println("3) Supprimer un Abonnement");
		System.out.println("----------------------------Client---------------------------------------------");
		System.out.println("4) Ajouter un Client");
		System.out.println("5) Modifier un Client");
		System.out.println("6) Supprimer un Client");
		System.out.println("----------------------------Periodicite----------------------------------------");
		System.out.println("7) Ajouter une périodicité");
		System.out.println("8) Modifier une périodicité");
		System.out.println("9) Supprimer une périodicité");
		System.out.println("----------------------------Revue----------------------------------------------");
		System.out.println("10) Ajouter une Revue");
		System.out.println("11) Modifier une Revue");
		System.out.println("12) Supprimer une Revue");
		
		action = sc.nextInt();
		
		switch(action){
		
		case 1:{
			System.out.println("Pour ajouter un Abonnement, entrez l'id du client : ");
			id_client = sc.nextInt();
			System.out.println("Entrez l'id de la revue : ");
			id_revue = sc.nextInt();
			System.out.println("Entrez la date de début (sous la forme : dd/MM/yyyy ) : ");
			date_debut = sc.next();
			DateTimeFormatter formatage = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			System.out.println("Entrez la date de fin (sous la forme : dd/MM/yyyy ) : ");
			date_fin = sc.next();
			DateTimeFormatter formatage2 = DateTimeFormatter.ofPattern("dd/MM/yyyy");

			
			
			//Abonnement Abo = new Abonnement(id_client, id_revue, date_debut, date_fin);
			//Abo.AjoutAbonnement(id_client, id_revue, date_debut, date_fin);
			System.out.println("L'abonnement à bien été créé. ");
			break;
		}
		case 2:{
			System.out.println("Pour modifier un Abonnement, entrez l'id du client : ");
			id_client = sc.nextInt();
			System.out.println("Entrez l'id de la revue : ");
			id_revue = sc.nextInt();
			System.out.println("Entrez la date de début (sous la forme : AAAA-MM-JJ ) : ");
			date_debut = sc.next();
			System.out.println("Entrez la date de fin (sous la forme : AAAA-MM-JJ ) : ");
			date_fin = sc.next();
			
			
			//Abonnement Abo = new Abonnement(id_client, id_revue, date_debut, date_fin);
			//Abo.ModificationAbonnement(id_client, id_revue, date_debut, date_fin);
			System.out.println("L'abonnement à bien été modifié. ");
			break;
		}
		case 3:{
			System.out.println("Pour supprimer un Abonnement, entrez l'id du client : ");
			id_client = sc.nextInt();
			System.out.println("Entrez l'id de la revue : ");
			id_revue = sc.nextInt();
			
			
			AbonnementDAO Abo = new AbonnementDAO();
			//Abo.delete(id_client, id_revue);
			System.out.println("L'abonnement à bien été supprimé. ");
			break;
		}
		
		
		case 4:{
			System.out.println("Pour ajouter un client, entrez un id de client : ");
			id_client = sc.nextInt();
			System.out.println("Entrez un nom");
			nom_client = sc.next();
			System.out.println("Entrez un prenom");
			prenom_client = sc.next();
			System.out.println("Entrez un numéro de rue");
			norue_client = sc.next();
			System.out.println("Entrez une voie");
			voie_client = sc.next();
			System.out.println("Entrez un code postal");
			cp_client = sc.next();
			System.out.println("Entrez une ville");
			ville_client = sc.next();
			System.out.println("Entrez un pays");
			pays_client = sc.next();
			
			
			Client Cli = new Client(id_client, nom_client, prenom_client, norue_client, voie_client, cp_client, ville_client, pays_client);
			Cli.create(id_client, nom_client, prenom_client, norue_client, voie_client, cp_client, ville_client, pays_client);
			break;
			
			
			
		}
		
		case 5:{
			System.out.println("Pour modifier un client, entrez un id de client : ");
			id_client = sc.nextInt();
			System.out.println("Entrez un nom");
			nom_client = sc.next();
			System.out.println("Entrez un prenom");
			prenom_client = sc.next();
			System.out.println("Entrez un numéro de rue");
			norue_client = sc.next();
			System.out.println("Entrez une voie");
			voie_client = sc.next();
			System.out.println("Entrez un code postal");
			cp_client = sc.next();
			System.out.println("Entrez une ville");
			ville_client = sc.next();
			System.out.println("Entrez un pays");
			pays_client = sc.next();
			
			
			Client Cli = new Client(id_client);
			Cli.update(id_client, nom_client, prenom_client, norue_client, voie_client, cp_client, ville_client, pays_client);
			System.out.println("Le client à bien été modifié. ");
			break;
		}
		case 6:{
			System.out.println("Pour Supprimer un client, entrez un id de client : ");
			id_client = sc.nextInt();
			
			
			ClientDAO Cli = new ClientDAO();
			Cli.delete(id_client);
 
			break;
		}
		
			
			
		case 7:{
			System.out.println("Pour ajouter une p�riodicit�, entrez l'id de la p�riodicit� : ");
			id_periodicite = sc.nextInt();
			System.out.println("Entrez le libell� de la p�riodicit� : ");
			lib_period = sc.next();
			
			Periodicite Period = new Periodicite(id_periodicite, lib_period);
			Period.create(id_periodicite, lib_period);
			System.out.println("la p�riodicit� � bien �t� cr��e. ");
			break;
			}
		
		
		case 8:{
			System.out.println("Pour modifier une périodicité, entrez l'id de la périodicité : ");
			id_periodicite = sc.nextInt();
			System.out.println("Entrez le libellé de la périodicité : ");
			lib_period = sc.next();
			
			Metier.Periodicite Period = new Periodicite(id_periodicite, lib_period);
			Period.update(id_periodicite, lib_period);
			System.out.println("la périodicité à bien été modifiée. ");
			break;
		}
		
		
		case 9:{
			System.out.println("Pour supprimer une périodicité, entrez l'id de la périodicité : ");
			id_periodicite= sc.nextInt();
			
			Periodicite Period = new Periodicite(id_periodicite, libelle);
			Period.delete(id_periodicite);
			System.out.println("la périodicité à bien été suprimée. ");
			break;
		}
				
			
			
			
			
			
			case 10:{
				System.out.println("Pour ajouter une revue, entrez l'id de la revue : ");
				id_revue = sc.nextInt();
				System.out.println("Entrez le titre : ");
				titre_revue = sc.next();
				System.out.println("Entrez la desctiption : ");
				desc_revue = sc.next();
				System.out.println("Entrez le tarif : ");
				tarifnum_revue = sc.nextFloat();
				System.out.println("Entrez le visuel : ");
				visuel_revue = sc.next();
				System.out.println("Entrez l'id de la periodicite : ");
				id_periodicite = sc.nextInt();
				
				RevueDAO rev = new RevueDAO();
				rev.create(id_revue, titre_revue, desc_revue, tarifnum_revue, visuel_revue, id_periodicite);
				System.out.println("la revue à bien été créée. ");
				break;
				}
			
			case 11:{
				System.out.println("Pour modifier une revue, entrez l'id de la revue : ");
				id_revue = sc.nextInt();
				System.out.println("Entrez le titre : ");
				titre_revue = sc.next();
				System.out.println("Entrez la desctiption : ");
				desc_revue = sc.next();
				System.out.println("Entrez le tarif : ");
				tarifnum_revue = sc.nextFloat();
				System.out.println("Entrez le visuel : ");
				visuel_revue = sc.next();
				System.out.println("Entrez l'id de la periodicite : ");
				id_periodicite= sc.nextInt();
				
				RevueDAO rev = new RevueDAO();
				rev.update(id_revue, titre_revue, desc_revue, tarifnum_revue, visuel_revue, id_periodicite);
				System.out.println("la revue à bien été modifée. ");
				break;
				}
			
			case 12:{
				System.out.println("Pour supprimer une revue, entrez l'id de la revue : ");
				id_revue = sc.nextInt();
				
				RevueDAO rev = new RevueDAO();
				rev.delete(id_revue);
				System.out.println("la revue à bien été supprimée. ");
				break;
				}
			
		}
		
		
		}
	}

}
