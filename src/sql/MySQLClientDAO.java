package sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Connections.*;
import DAO.*;
import Metier.*;



public class MySQLClientDAO implements DAO<Client>{
	
	private static MySQLClientDAO instance;
	Connexion connect = new Connexion(); 
	private ArrayList<Client> donnees; 

	
	private MySQLClientDAO() {
		this.donnees = new ArrayList<Client>();
	}
	

	
	public static MySQLClientDAO getInstance() {
		if (instance == null) {
			instance = new MySQLClientDAO();
		}
		return instance;
	}



	@Override
	public Client getByIdClient() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public Client getById(int id) {
		Client cli = new Client(id, null, null, null, null, null, null, null); 
		try {
			Connection laConnexion = connect.creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("SELECT * FROM Client WHERE id_client=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();

			if (res.next()) {
				cli.setNom(res.getString("nom"));
				cli.setPrenom(res.getString("prenom"));
				cli.setNo_rue(res.getString("no_rue"));
				cli.setVoie(res.getString("voie"));
				cli.setcode_postal(res.getString("code_postal"));
				cli.setVille(res.getString("ville"));
				cli.setPays(res.getString("pays"));
				res.close();
			} else {
				System.out.println("Client inexistant !");
			}

			if (requete != null) {
				requete.close();
			}

		} catch (SQLException se) {
			System.out.println("Pb SQL " + se.getMessage());
		}

		return cli;
	}
	



	@Override
	public void create(Client objet) {
		try {
			Connection laConnexion = connect.creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("insert into Client (id_client) VALUES (?)");
			requete.setInt(1, objet.getid_client());
			requete.executeUpdate();
			laConnexion.close();
			} catch (SQLException sqle) {
				System.out.println("Pb dans select " + sqle.getMessage());
			}
	}



	@Override
	public void update(Client objet) {
		try {
			Connection laConnexion = connect.creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("update into Client where id_Client = ?");
			requete.setInt(1, objet.getid_client());
			requete.executeUpdate();
			laConnexion.close();
			} catch (SQLException sqle) {
				System.out.println("Pb dans select " + sqle.getMessage());
			}
	}



	@Override
	public void delete(Client objet) {
		try {
			Connection laConnexion = connect.creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("delete from Client where id_Client = ?");
			requete.setInt(1, objet.getid_client());
			requete.executeUpdate();
			laConnexion.close();
			} catch (SQLException sqle) {
				System.out.println("Pb dans select " + sqle.getMessage());}
	}



	@Override
	public List<Client> findClient() { 
		try {
			this.donnees.clear();
			Connection laConnexion = Connexion.creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("SELECT * FROM Client ORDER BY id_client");
			ResultSet res = requete.executeQuery();
			while (res.next()) {
				Client cli = new Client(0, null, null, null, null, null, null, null);
				System.out.println(res.getInt("id_client"));
				Client.setId_client(res.getInt("id_client"));
				System.out.println(res.getString("libelle"));
				cli.setVille(res.getString("ville"));
				this.donnees.add(cli);
				
			} 
			if (requete != null) {
				requete.close();
			}
			

		} catch (SQLException se) {
			System.out.println("Pb SQL " + se.getMessage());
		}

		return this.donnees;
	}



	@Override
	public Periodicite getByIdPeriodicite() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public ArrayList<Revue> findAll() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public Revue getByIdRevue() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<Integer> findRevue() {
		// TODO Auto-generated method stub
		return null;
	}






}
