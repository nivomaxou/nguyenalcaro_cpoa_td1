package sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Connections.*;
import DAO.*;
import Metier.*;



public class MySQLPeriodiciteDAO implements DAO<Periodicite>{
	
	private static MySQLPeriodiciteDAO instance;
	Connexion connect = new Connexion(); 
	private ArrayList<Periodicite> donnees; 

	
	private MySQLPeriodiciteDAO() {
		this.donnees = new ArrayList<Periodicite>();
	}
	

	
	public static MySQLPeriodiciteDAO getInstance() {
		if (instance == null) {
			instance = new MySQLPeriodiciteDAO();
		}
		return instance;
	}



	@Override
	public Periodicite getByIdPeriodicite() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public Periodicite getById(int id) {
		Periodicite period = new Periodicite(id, null); 
		try {
			Connection laConnexion = connect.creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("SELECT * FROM Periodicite WHERE id_periodicite=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();

			if (res.next()) {
				period.setId(res.getInt("id_periodicite"));
				period.setlibelle(res.getString("libelle"));
				res.close();
			} else {
				System.out.println("Période inexistante !");
			}

			if (requete != null) {
				requete.close();
			}

		} catch (SQLException se) {
			System.out.println("Pb SQL " + se.getMessage());
		}

		return period;
	}



	@Override
	public void create(Periodicite objet) {
		try {
			Connection laConnexion = connect.creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("insert into Periodicite (libelle) VALUES (?)");
			requete.setString(1, objet.getlibelle());
			requete.executeUpdate();
			laConnexion.close();
			} catch (SQLException sqle) {
				System.out.println("Pb dans select " + sqle.getMessage());
			}
	}



	@Override
	public void update(Periodicite objet) {
		try {
			Connection laConnexion = connect.creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("update Periodicite set libelle = ? where id_periodicite = ?");
			requete.setString(1, objet.getlibelle());
			requete.setInt(2, objet.getId());
			requete.executeUpdate();
			laConnexion.close();
			} catch (SQLException sqle) {
				System.out.println("Pb dans select " + sqle.getMessage());
			}
	}



	@Override
	public void delete(Periodicite objet) {
		try {
			Connection laConnexion = connect.creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("delete from Periodicite where id_periodicite = ?");
			requete.setInt(1, objet.getId());
			requete.executeUpdate();
			laConnexion.close();
			} catch (SQLException sqle) {
				System.out.println("Pb dans select " + sqle.getMessage());}
	}



	@Override
	public ArrayList<Periodicite> findAll() { 
		try {
			this.donnees.clear();
			Connection laConnexion = connect.creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("SELECT * FROM Periodicite ORDER BY id_periodicite");
			ResultSet res = requete.executeQuery();
			while (res.next()) {
				Periodicite period = new Periodicite(0, null);
				System.out.println(res.getInt("id_periodicite"));
				period.setId(res.getInt("id_periodicite"));
				System.out.println(res.getString("libelle"));
				period.setlibelle(res.getString("libelle"));
				this.donnees.add(period);
				
			} 
			if (requete != null) {
				requete.close();
			}
			

		} catch (SQLException se) {
			System.out.println("Pb SQL " + se.getMessage());
		}

		return this.donnees;
	}



	@Override
	public Revue getByIdRevue() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<Integer> findRevue() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Client getByIdClient() {
		// TODO Auto-generated method stub
		return null;
	}



}
