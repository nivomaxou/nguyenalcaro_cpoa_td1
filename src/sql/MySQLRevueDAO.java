package sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Connections.*;
import DAO.*;
import Metier.*;

public class MySQLRevueDAO implements DAO<Revue>{
	
	private static MySQLRevueDAO instance;
	Connexion connect = new Connexion(); 
	private ArrayList<Revue> donnees; 
	
	private MySQLRevueDAO() {
		this.donnees = new ArrayList<Revue>();
	}
	
	
	public static MySQLRevueDAO getInstance() {
		if (instance == null) {
			instance = new MySQLRevueDAO();
		}
		return instance;
	}


	@Override
	public Revue getByIdRevue() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Revue getById(int id) {
		Revue rev = new Revue(id);
		try {
			Connection laConnexion = connect.creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("SELECT * FROM Revue WHERE id_revue=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();
			if (res.next()) {
				rev.setid_revue(res.getInt("id_revue"));
				rev.setTitre(res.getString("titre"));
				rev.setDescription(res.getString("description"));
				rev.settarif_numero(res.getDouble("tarif_numero"));
				rev.setVisuel(res.getString("visuel"));
				rev.setId_periodicite(res.getInt("id_periodicite"));
				res.close();
			} else {
				System.out.println("Revue inexistante !");
			}
			
			if (requete != null) {
				requete.close();
			}
			
		} catch (SQLException se) {
			System.out.println("Pb SQL " + se.getMessage());
		}

		return rev;
	}


	@Override
	public void create(Revue objet) {
		try {
			Connection laConnexion = connect.creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("insert into Revue (titre, description, tarif_numero, visuel, id_periodicite) VALUES (?,?,?,?,?)");
			requete.setString(1, objet.getTitre());
			requete.setString(2, objet.getDescription());
			requete.setDouble(3, objet.gettarif_numero());
			requete.setString(4, objet.getVisuel());
			requete.setInt(5, objet.getId_periodicite());
			requete.executeUpdate();
			laConnexion.close();
			} catch (SQLException sqle) {
				System.out.println("Pb dans select " + sqle.getMessage());
			}
	}


	@Override
	public void update(Revue objet) {
		try {
			Connection laConnexion = connect.creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement(
					"UPDATE Revue SET titre=?, description=?, tarif_numero=?, visuel=?, id_periodicite=? WHERE id_revue=?");
			requete.setString(1, objet.getTitre());
			requete.setString(2, objet.getDescription());
			requete.setDouble(3, objet.gettarif_numero());
			requete.setString(4, objet.getVisuel());
			requete.setLong(5, objet.getId_periodicite());
			requete.setLong(6, objet.getid_revue());
			requete.executeUpdate();

		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}
	}


	@Override
	public void delete(Revue objet) {
		try {
			Connection laConnexion = connect.creeConnexion();
			PreparedStatement requete = laConnexion.prepareStatement("DELETE FROM Revue WHERE id_revue=?");
			requete.setLong(1, objet.getid_revue());
			requete.executeUpdate();

		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}
	}


	@Override
	public ArrayList<Revue> findAll() {
		try {
			this.donnees.clear();
			Connection laConnexion = connect.creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("SELECT * FROM Revue ORDER by id_revue");
			ResultSet res = requete.executeQuery();
			while (res.next()) {
				Revue rev = new Revue(0); 
				rev.setid_revue(res.getInt("id_revue"));
				rev.setTitre(res.getString("titre"));
				rev.setDescription(res.getString("description"));
				rev.settarif_numero(res.getFloat("tarif_numero"));
				rev.setVisuel(res.getString("visuel"));
				rev.setId_periodicite(res.getInt("id_periodicite"));
				this.donnees.add(rev);
				
			} 
			if (requete != null) {
				requete.close();
			}

		} catch (SQLException se) {
			System.out.println("Pb SQL " + se.getMessage());
		}

		return this.donnees;
	}


	@Override
	public List<Integer> findRevue() {
		ArrayList<Integer> rev = new ArrayList<Integer>(); 
		try {
			this.donnees.clear();
			Connection laConnexion = connect.creeConnexion(); 
			PreparedStatement requete = laConnexion.prepareStatement("SELECT id_revue FROM Revue");
			ResultSet res = requete.executeQuery();
			while (res.next()) {
				rev.add(res.getInt("id_revue")); 
			}
			if (requete != null) {
				requete.close();
			}

		} catch (SQLException se) {
			System.out.println("Pb SQL " + se.getMessage());
		}

		return rev;
	}


	@Override
	public Periodicite getByIdPeriodicite() {
		// TODO Auto-generated method stub
		return null;
	}
}
